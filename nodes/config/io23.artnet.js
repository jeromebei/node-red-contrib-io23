const __Artnet=require('artnet');
module.exports = function(RED) {
  //==========================================================================
  function io23_Artnet(config) {
      RED.nodes.createNode(this,config);
      this.name=config.name;
      this.host=config.host;
      this.leds=parseInt(config.leds);
      this.startUniverse=parseInt(config.startUniverse);
      this.universeCount=parseInt(config.universeCount);
      this.endUniverse=parseInt(config.endUniverse);
    //--------------------------------------------------------------------------
  }
  //============================================================================
  RED.nodes.registerType("io23.artnet",io23_Artnet);
  //============================================================================
}
