const fs = require('fs');
const fetch = require('node-fetch');
const fileType = require('file-type');
const resizeImg = require('resize-img');
const jpeg = require('jpeg-js');
const PNG = require("pngjs").PNG;

const Common = require("../../lib/common.js");

module.exports = function(RED) {
  //==========================================================================
  function io23_ImgToChn(config) {
      RED.nodes.createNode(this,config);
      var node = this;
      //----------------------------------------------------------------------
      node.on('input', function(msg) {
        msg.source = msg.source || config.source;
        msg.cropX  = msg.cropX || parseInt(config.cropX) || 0;
        msg.cropY  = msg.cropY || parseInt(config.cropY) || 0;
        msg[config.outProperty]=[];
        if (!msg.source) {
          msg.status = "error";
          msg.code = "source not specified";
          return Common.forward(node, msg, `source not specified`, Common.status.error);
        }
        getImage(msg.source,(data,type)=>{
          var mimeStr=null;
          switch (type.mime) {
            case "image/jpeg": mimeStr = "jpg"; break;
            case "image/png":  mimeStr = "png"; break;
            default: null; break;
          }
          if (!mimeStr) {
            msg.status = "error";
            msg.code = `unknown mime type ${type.mime}`;
            return Common.forward(node, msg, `unknown mime type ${type.mime}`, Common.status.error);
          }
          cropImage(data,mimeStr,msg,(data)=>{
            decomposeImage(data,mimeStr,msg,(channels)=>{
              msg[config.outProperty]=channels;
              return Common.forward(node, msg, `done`, Common.status.ok);
            },(err)=>{
              msg.status = "error";
              msg.code = `cannot decompose ${err}`;
              return Common.forward(node, msg, `cannot decompose`, Common.status.error);
            });
          },(err)=>{
            msg.status = "error";
            msg.code = `cannot crop ${err}`;
            return Common.forward(node, msg, `cannot crop`, Common.status.error);
          });
        },(err)=>{
          msg.status = "error";
          msg.code = `cannot load ${err}`;
          return Common.forward(node, msg, `cannot load`, Common.status.error);
    });

      });
    //--------------------------------------------------------------------------
    function decomposeImage(data,mimeStr,msg,done,failed) {
      Common.process(node, `decomposing ${mimeStr}`);
      if (mimeStr=="jpg") {
        try {
          var jpegData = jpeg.decode(data,{useTArray: true, formatAsRGBA: false});
          if(!jpegData) return failed(`no data`);
          jpegData.data = jpegData.data.map((d)=>{return d>254?254:d});

          return done(Array.from(jpegData.data));
        } catch(err) {
          return failed(err);
        }
      } else if (mimeStr=="png") {
        try {
          new PNG({ colorType: 2, bgColor: {red:0,rgreen:0,blue:0}}).parse(data, function (err, data) {
            if (err) return failed(err);
            data = (Array.from(new Uint8Array(data.data)));
            var i = data.length;
            while (i--) (i + 1) %4  === 0 && data.splice(i, 1); // remove alpha channel
            data = data.map((d)=>{return d>254?254:d});
            return done(data);
          });
        } catch(err) {
          return failed(err);
        }
      } else {
        return failed(`unknown mime type ${mimeStr}`);
      }
    }
    //--------------------------------------------------------------------------
    function cropImage(data,mimeStr,msg,done,failed) {
      if (!msg.cropX || !msg.cropY || msg.cropX==0 || msg.cropY==0) {
        Common.process(node, `nothing to crop`);
        return done(data);
      }
      Common.process(node, `cropping`);
      try {
        resizeImg(data, {width: msg.cropX, height: msg.cropY, format: mimeStr}).then((data)=>{
            return done(data);
        }).catch((err)=>{
          return failed(err);
        });
    } catch (err) {
      return failed(err);
    }       
    }
    //--------------------------------------------------------------------------
    function getImage(source, done, failed){
      Common.process(node, `fetching`);
      if (source.toLowerCase().startsWith("http")) {
        fetch(source).then((res) => {
          if (!res.ok) return failed(res.statusText);
          res.buffer().then((data)=>{
            fileType.fromBuffer(data).then((type)=>{return done(data,type);}).catch((err)=>{return failed(err);});
          }).catch((err)=>{return failed(err);});
        }).catch((err)=>{return failed(err);});
      } else {
        fs.readFile(source, function(err, data) {
          if (err) return failed(err);
          fileType.fromBuffer(data).then((type)=>{return done(data,type);})
          .catch((err)=>{return failed(err);});
        });
      }
    }
    //--------------------------------------------------------------------------
  }
  //============================================================================
  RED.nodes.registerType("io23.imgToChn",io23_ImgToChn);
  //============================================================================
}
