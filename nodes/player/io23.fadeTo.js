const Common = require("../../lib/common.js");

module.exports = function(RED) {
  //==========================================================================
  function io23_FadeTo(config) {
      RED.nodes.createNode(this,config);
      var node = this;
      var next = null;
      var from = [], to = [];
      var count = 0;
      //----------------------------------------------------------------------
      node.on('input', function(msg) {
        if (next) clearTimeout(next);
        from = [...msg[config.from]];
        to   = [...msg[config.to]];
        msg[config.outProperty]=[];
        config.sendInterval = parseInt(config.sendInterval);
        config.time = parseInt(config.time);

        if (!Array.isArray(from)) return Common.forward(node, msg, `msg.${config.from} not an array`, Common.status.warn);
        if (!Array.isArray(to))   return Common.forward(node, msg, `msg.${config.to} not an array`, Common.status.warn);
        if (config.sendInterval > config.time)  {
          msg[config.outProperty]=to;
          return Common.forward(node, msg, `send interval (${config.sendInterval}) > time (${config.time})`, Common.status.warn);
        }

        count = Math.ceil(config.time / config.sendInterval);
        Common.process(node, `fading ${from.length} channels in ${count} steps`);
        fade(from, to, count, ()=>{
            msg.status="done";
            return Common.forward(node, msg, `done`, Common.status.ok);
        }); 
      });
    //--------------------------------------------------------------------------
    function fade(from, to, count, done) {
      if (count==0) return done();
        for (var idx=0; idx<from.length;idx++) {
            var diff = Math.abs(from[idx] - to[idx]);
            var step = (diff / count);
            from[idx] = Math.round(from[idx] + (to[idx] > from[idx] ? step : from[idx] == to[idx] ? 0 : 0-step));
        }
        msg={};
        msg[config.outProperty]=from;
        node.send(msg);
        count--;
        next = setTimeout(fade, config.sendInterval, from.slice(), to.slice(), count, done);
    }
    //--------------------------------------------------------------------------
  }

  //============================================================================
  RED.nodes.registerType("io23.fadeTo",io23_FadeTo);
  //============================================================================
}
