const Common = require("../../lib/common.js");
const ArtNetClient = require('../../lib/artnet-client.js');

module.exports = function(RED) {
  //==========================================================================
  function io23_Player(config) {
      RED.nodes.createNode(this,config);
      var node = this;
      config.port=parseInt(config.port);
      config.devices = config.devices.split(",").filter((el)=>{return el && el!=""}).map((id)=>{return RED.nodes.getNode(id);}).filter((el)=>{return el!=null;}); 
      config.devices.sort((a,b)=>{return parseInt(a.startUniverse) >= parseInt(b.startUniverse) ? 1:-1});
      config.devices.forEach((device)=>{device.client = ArtNetClient.createClient(device.host, config.port || 6454);});
      //----------------------------------------------------------------------
      node.on('input', function(msg) {
        if (!Array.isArray(msg[config.inProperty])) return Common.forward(node, msg, `nothing to process`, Common.status.ok); 
        if (msg.status=="done") return Common.forward(node, msg, `done`, Common.status.ok); 
        if (msg[config.inProperty].length==0) return Common.forward(node, msg, `no data`, Common.status.ok); 
        Common.process(node, `sending ${msg[config.inProperty].length} channels`);
        msg.devices=config.devices;
        msg.devices.forEach((device,idx)=>{
          //get all channels for current device
          var channelsForThisDevice=msg[config.inProperty].splice(0,device.leds*3);
          //console.log(`device ${idx} will receive ${channelsForThisDevice.length} channels total`);
          device.universes=[];

          //split into universes, make sure all 3 led channels stay in same universe
          for (var universeCounter=0; universeCounter<device.universeCount; universeCounter++) {
            device.universes[universeCounter]=channelsForThisDevice.splice(0,510);
            //console.log(`device ${idx} will receive ${device.universes[universeCounter].length} channels in universe ${universeCounter+device.startUniverse}, remaining channels: ${channelsForThisDevice.length}`);
            //device.artnet.set(universeCounter+device.startUniverse,1,device.universes[universeCounter]); 
            //console.log(`sending ${device.universes[universeCounter].length} channels to universe ${universeCounter+device.startUniverse}`);
            device.client.send(universeCounter+device.startUniverse,device.universes[universeCounter]); 
          }
        });
        return config.forwardState ? Common.forward(node, msg, `done`, Common.status.ok) : Common.done(node, `done`, Common.status.ok);
      });
    //--------------------------------------------------------------------------
  }
  //============================================================================
  RED.nodes.registerType("io23.player",io23_Player);
  //============================================================================
}
