const Common = require("../../lib/common.js");

module.exports = function(RED) {
  //==========================================================================
  function io23_Dimmer(config) {
      RED.nodes.createNode(this,config);
      var node = this;
      //----------------------------------------------------------------------
      node.on('input', function(msg) {
        if (!Array.isArray(msg[config.inProperty]) || msg[config.inProperty].length==0) return Common.forward(node, msg, `nothing to process`, Common.status.ok); 
        msg.luminosity  = msg.luminosity || parseInt(config.luminosity) || 100;
        if (msg.luminosity==100) return Common.forward(node, msg, `no luminosity change needed`, Common.status.ok); 
        if (msg.luminosity<0) msg.luminosity=0;
        if (msg.luminosity>100) msg.luminosity=100;

        msg[config.outProperty]=[...msg[config.inProperty]];
        msg[config.outProperty]=msg[config.outProperty].map((ch)=>{return Math.round(ch/100*msg.luminosity);});
        return Common.forward(node, msg, `done`, Common.status.ok); 

      });
    //--------------------------------------------------------------------------
  }

  //============================================================================
  RED.nodes.registerType("io23.dimmer",io23_Dimmer);
  //============================================================================
}
