const Common = require("../../lib/common.js");

module.exports = function(RED) {
  //==========================================================================
  function io23_Saturation(config) {
      RED.nodes.createNode(this,config);
      var node = this;
      //----------------------------------------------------------------------
      node.on('input', function(msg) {
        if (!Array.isArray(msg[config.inProperty]) || msg[config.inProperty].length==0) return Common.forward(node, msg, `nothing to process`, Common.status.ok); 
        msg.saturation  = msg.saturation || parseInt(config.saturation) || 100;
        if (msg.saturation<0) msg.saturation=0;
        if (msg.saturation>100) msg.saturation=100;

        msg[config.outProperty]=[...msg[config.inProperty]];

        for (var i=0; i<msg[config.outProperty].length/3; i++) {
            var color = [msg[config.outProperty][i*3],msg[config.outProperty][i*3+1],msg[config.outProperty][i*3+2]];
            color = RGBtoHSV(color);
            color[1] = 100/msg.saturation;  //change saturation
            color = HSVtoRGB(color);
            msg[config.outProperty][i*3]  =color[0];
            msg[config.outProperty][i*3+1]=color[1];
            msg[config.outProperty][i*3+2]=color[2];
        }
        return Common.forward(node, msg, `done`, Common.status.ok); 

      });
    //--------------------------------------------------------------------------
    RGBtoHSV=function(color) {
        var r,g,b,h,s,v;
        r= color[0];
        g= color[1];
        b= color[2];
        min = Math.min( r, g, b );
        max = Math.max( r, g, b );


        v = max;
        delta = max - min;
        if( max != 0 )
            s = delta / max;        // s
        else {
            // r = g = b = 0        // s = 0, v is undefined
            s = 0;
            h = -1;
            return [h, s, undefined];
        }
        if( r === max )
            h = ( g - b ) / delta;      // between yellow & magenta
        else if( g === max )
            h = 2 + ( b - r ) / delta;  // between cyan & yellow
        else
            h = 4 + ( r - g ) / delta;  // between magenta & cyan
        h *= 60;                // degrees
        if( h < 0 )
            h += 360;
        if ( isNaN(h) )
            h = 0;
        return [h,s,v];
    };

HSVtoRGB= function(color) {
        var i;
        var h,s,v,r,g,b;
        h= color[0];
        s= color[1];
        v= color[2];
        if(s === 0 ) {
            // achromatic (grey)
            r = g = b = v;
            return [r,g,b];
        }
        h /= 60;            // sector 0 to 5
        i = Math.floor( h );
        f = h - i;          // factorial part of h
        p = v * ( 1 - s );
        q = v * ( 1 - s * f );
        t = v * ( 1 - s * ( 1 - f ) );
        switch( i ) {
            case 0:
                r = v;
                g = t;
                b = p;
                break;
            case 1:
                r = q;
                g = v;
                b = p;
                break;
            case 2:
                r = p;
                g = v;
                b = t;
                break;
            case 3:
                r = p;
                g = q;
                b = v;
                break;
            case 4:
                r = t;
                g = p;
                b = v;
                break;
            default:        // case 5:
                r = v;
                g = p;
                b = q;
                break;
        }
        return [r,g,b];
    }    
    //--------------------------------------------------------------------------
}

  //============================================================================
  RED.nodes.registerType("io23.saturation",io23_Saturation);
  //============================================================================
}
