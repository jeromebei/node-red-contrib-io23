const Common = require("../../lib/common.js");

module.exports = function(RED) {
  //==========================================================================
  function io23_Matrix(config) {
      RED.nodes.createNode(this,config);
      var node = this;
      //----------------------------------------------------------------------
      node.on('input', function(msg) {
        if (!Array.isArray(msg[config.inProperty]) || msg[config.inProperty].length==0) return Common.forward(node, msg, `nothing to process`, Common.status.ok); 
        msg.rows  = msg.rows || parseInt(config.rows) || 32;
        msg.columns  = msg.columns || parseInt(config.columns) || 8;
        
        var revLen=msg.columns * 3;
        var channelsIn =[...msg[config.inProperty]];
        var channelsOut=[];

        for (var i=0; i<msg.rows; i++) {
            var rowChannels=channelsIn.splice(0,revLen);
            if (i% 2 == 0) {
                //switch leds in row
                var rowLeds=[];
                for (var l=0; l<rowChannels.length/3; l++) rowLeds[l]=[rowChannels[l*3],rowChannels[l*3+1],rowChannels[l*3+2]];
                rowLeds.reverse();
                rowChannels=[].concat(...rowLeds);
            }
            channelsOut=channelsOut.concat(rowChannels);
        }
        msg[config.outProperty]=[...channelsOut];
        return Common.forward(node, msg, `done`, Common.status.ok); 

      });
    //--------------------------------------------------------------------------
}

  //============================================================================
  RED.nodes.registerType("io23.matrix",io23_Matrix);
  //============================================================================
}
