var COMMON = {
    status: {ok:"green",warn:"yellow",error:"red",process:"blue"},
    statusQueue: [],
    //----------------------------------------------------------------------
    forward: (node,msg,txt,status)=>{
        node.status({fill:status,shape:"dot",text:txt});
        COMMON.__queueStatus(node);
        return node.send(msg);
    },
    //----------------------------------------------------------------------
    done: (node,txt,status)=>{
        node.status({fill:status,shape:"dot",text:txt});
        COMMON.__queueStatus(node);
    },
    //----------------------------------------------------------------------
    process: (node,txt)=>{
        node.status({fill:COMMON.status.process, shape:"ring", text:txt});
    },
    //----------------------------------------------------------------------
    //----------------------------------------------------------------------
    __removeStatus:(node)=>{
        var inQueue = COMMON.statusQueue.find((el)=>{return el.id==node.id;});
        if (inQueue) {
            clearTimeout(inQueue.timeout);
            COMMON.statusQueue=COMMON.statusQueue.filter((el)=>{return el.id!=node.id});      
        }  
    },
    //----------------------------------------------------------------------
    __queueStatus:(node)=>{
        COMMON.__removeStatus(node);
        var timeout=setTimeout((node)=>{
            COMMON.statusQueue=COMMON.statusQueue.filter((el)=>{return el.id!=node.id});            
            node.status({});
        },2000,node);
        COMMON.statusQueue.push({id:node.id,timeout:timeout});
    },
    //----------------------------------------------------------------------
}
module.exports = COMMON;