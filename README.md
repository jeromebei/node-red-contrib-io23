# node-red-contrib-io23
A collection of node-red nodes for the io23.io artnet devices.

The nodes allow you to send Artnet channels to the io23artnet hardware. 

Refer to [io23.io](https://io23.io) for details.

# Installation
[![NPM](https://nodei.co/npm/node-red-contrib-io23.png?downloads=true)](https://nodei.co/npm/node-red-contrib-io23/)

You can install the nodes using node-red's "Manage palette" in the side bar.

Or run the following command in the root directory of your Node-RED installation

    npm install node-red-contrib-io23 --save

# Dependencies
The nodes are tested with `Node.js v12.18.2` and `Node-RED v1.2.9`.

Please refer to package.json for dependency details.

# License
[<img src="https://www.gnu.org/graphics/gplv3-127x51.png" alt="GPLv3" >](http://www.gnu.org/licenses/gpl-3.0.html)

node-red-contrib-io23 is a free software project licensed under the GNU General Public License v3.0 (GPLv3) by Jérôme Bei.
